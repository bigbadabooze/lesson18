package zone.haze.restaurant.data

import zone.haze.restaurant.data.exceptions.directory.DirectoryIsNotFileException
import zone.haze.restaurant.data.exceptions.file.FileNotReadableException
import zone.haze.restaurant.data.models.CookerFabric
import zone.haze.restaurant.data.models.DishFabric
import zone.haze.restaurant.kitchen.types.Cooker
import zone.haze.restaurant.kitchen.types.Dish
import zone.haze.restaurant.kitchen.types.DishType
import zone.haze.restaurant.utils.file.Files
import java.io.File
import java.io.FileNotFoundException
import java.util.regex.Matcher
import java.util.regex.Pattern

class DB private constructor(options: Options) {
    private lateinit var database: HashMap<Day, HashMap<DishType, KitchenCollection<Cooker, Dish>>>
    private val db: File

    companion object {
        var currentDay: Day = Day.Monday
            private set(value) {
                field = value
            }

        private val files: Files = Files.instance

        fun getConnection(options: Options): DB {
            // You can add some other initializations here, before new DB object has been created
            if (!options.containsKey("db")) options["db"] = "./db.txt"

            return DB(options)
        }

    }

    init {
        val dbString = options["db"]

        db = if (dbString!!.startsWith("./"))
            files.getWorkDirectoryFile(dbString)
        else files.getDataFile(dbString)

        db.exists() || throw FileNotFoundException(
                String.format(
                        "There is no such file or folder [%s]",
                        db.path.toString()
                )
        )

        db.isFile || throw DirectoryIsNotFileException("DB must bee an file, not a directory")

        db.canRead() || throw FileNotReadableException(
                String.format(
                        "Can't read DB file. Are you sure that [%s] path is correct?",
                        db.path.toString()
                )
        )
    }


    fun connect(): DB {
        database = HashMap(7)

        enumValues<Day>().forEach { day ->
            database[day] = HashMap()

            enumValues<DishType>().forEach {
                database[day]!![it] = KitchenCollection()
            }
        }

        fillDB()

        return this
    }

    private fun addToCollection(day: Day, type: DishType, cookerOrDish: Any) {
        if (cookerOrDish is Dish) {
            database[day]!![type]!!.addDish(cookerOrDish)
        }

        if (cookerOrDish is Cooker) {
            database[day]!![type]!!.addCooker(cookerOrDish)
        }
    }

    private fun fillDB() {
        val cookerPattern = Pattern.compile("^([а-яёa-zА-ЯЁA-Z ]+):([a-zA-Z]+)\$")
        val dishPattern = Pattern.compile("^([а-яёa-zА-ЯЁA-Z ]+):(\\d+)[а-яёa-zА-ЯЁA-Z]*\\.?\\s+(\\d+)[а-яёa-zА-ЯЁA-Z]*\\.?:([a-zA-Z]+)$")
        var matcher: Matcher

        db.readLines().forEach { dbString ->
            if (Day.isDay(dbString)) {
                currentDay = Day.toDay(dbString)
                return@forEach
            }

            matcher = cookerPattern.matcher(dbString)

            if (matcher.find()) {
                if (DishType.isDishType(matcher.group(2))) {
                    addToCollection(
                            currentDay,
                            DishType.toDishType(matcher.group(2)),
                            CookerFabric.createCooker(
                                    matcher.group(1), // Name
                                    matcher.group(2) // Specialization
                            )
                    )
                }

                return@forEach
            }

            matcher = dishPattern.matcher(dbString)

            if (matcher.find()) {
                if (DishType.isDishType(matcher.group(4))) {
                    val price = matcher.group(2).toDouble() + matcher.group(3).toDouble() * 0.01

                    addToCollection(
                            currentDay,
                            DishType.toDishType(matcher.group(4)),
                            DishFabric.createDish(
                                    matcher.group(1), // Title
                                    price.toString(), // Price
                                    matcher.group(4) // Type
                            )
                    )
                }
            }
        }
    }

    fun data(): HashMap<Day, HashMap<DishType, KitchenCollection<Cooker, Dish>>> = database

    fun getCollection(day: Day): java.util.HashMap<DishType, KitchenCollection<Cooker, Dish>>? {
        return data()[day]
    }

}
