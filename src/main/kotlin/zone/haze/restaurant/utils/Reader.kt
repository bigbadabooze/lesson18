package zone.haze.restaurant.utils

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

object Reader {
    private var stream: InputStreamReader = InputStreamReader(System.`in`)
    private val reader: BufferedReader = BufferedReader(stream)

    fun reader(): BufferedReader = reader

    fun setInputSource(input: InputStream) {
        stream = InputStreamReader(input)
    }

    fun resetInputSource() {
        stream = InputStreamReader(System.`in`)
    }

    fun line(): String {
        val line = reader.readLine().trim()

        if (line.toLowerCase() == "exit" || line.toLowerCase() == "выход") {
            println("Спасибо, что воспользовались нашими авиалиниями.")

            System.exit(0)
        }

        return line
    }

    fun int(): Int = line().toInt()

    fun toInt(input: String): Int = input.toInt()

    fun double(): Double = line().toDouble()

    fun toDouble(input: String): Double = input.toDouble()

    fun regex(pattern: String): Pattern = Pattern.compile(pattern)

    fun regex(pattern: String, bitOptions: Int): Pattern = Pattern.compile(pattern, bitOptions)

    fun matcher(pattern: String): Matcher = regex(pattern).matcher(line())

}
