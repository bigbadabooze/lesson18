package zone.haze.restaurant.utils.file

import java.io.File

enum class Files {
    instance;

    private val paths = Paths.instance

    private fun Paths.dataString(pathParts: Array<out String>): String =
        this.dataString(true, pathParts.joinToString(separator = paths.separator, prefix = paths.separator))

    fun gluePathString(vararg pathParts: String): String =
        pathParts.joinToString(separator = paths.separator, prefix = paths.separator)

    fun getDataFile(vararg pathParts: String): File {
        return File(paths.dataString(pathParts))
    }

    fun getWorkDirectoryFile(path: String): File {
        return File(path)
    }

}
