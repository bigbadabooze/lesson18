package zone.haze.restaurant.utils.file;

import java.nio.file.Path;

public enum Paths {
    instance;
    private final String os = System.getProperty("os.name").toLowerCase().contains("window") ? "windows" : "other";

    private final Path app;

    {
        String temp = Paths.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        if ("windows".equals(os)) {
            temp = temp.substring(0, temp.length() - 2);
        }

        app = java.nio.file.Paths.get(
                temp
        ).getParent();
    }

    private final String separator = System.getProperty("file.separator");

    public Path combine(String part, String... parts) {
        return java.nio.file.Paths.get(part, parts);
    }

    public Path appPath() {
        return app;
    }

    public String appString() {
        return app.toString();
    }

    public String getSeparator() {
        return separator;
    }

    private Path moduleDirCombiner(String moduleDir, String... parts) {
        return combine(combine(appString(), moduleDir).toString(), parts);
    }

    private String moduleDirString(boolean clear, Path module) {
        if (!clear)
            return module.toString();

        // Actually not needed, I think
        return module.toString().replaceAll("(file:|jar:|dir:|directory:|uri:|url:)", "");
    }

    public Path libs(String... parts) {
        return moduleDirCombiner("libs", parts);
    }

    public String libsString(boolean clear, String... parts) {
        return moduleDirString(clear, libs(parts));
    }

    public String libsString(String... parts) {
        return libsString(true, parts);
    }

    public Path data(String... parts) {
        return moduleDirCombiner("data", parts);
    }

    public String dataString(boolean clear, String... parts) {
        return moduleDirString(clear, data(parts));
    }

    public String dataString(String... parts) {
        return libsString(true, parts);
    }

}
