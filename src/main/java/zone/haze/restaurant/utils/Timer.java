package zone.haze.restaurant.utils;

import java.util.HashMap;

public class Timer {
    private static HashMap<String, Timer> timers = new HashMap<>();
    private static Timer lastTimer = null;

    private String name;
    private long start = 0L;
    private long end = 0L;

    private Timer() {
    }

    public Timer(String name) {
        this.name = name;
        lastTimer = this;
        timers.put(name, this);
        start = System.currentTimeMillis();
    }

    public static long stop() {
        if (lastTimer != null) {
            lastTimer.end = System.currentTimeMillis();

            return lastTimer.end - lastTimer.start;
        }

        return -1;
    }

    public static long stop(String name) {
        Timer timer = null;

        if ((timer = timers.get(name)) != null) {
            timer.end = System.currentTimeMillis();

            return timer.end - timer.start;
        }

        return -1;
    }

    @Override
    public String toString() {
        return String.format("%s: %s секунд", name, (double) (end - start) / 1000);
    }

    public static void printAll() {
        timers.forEach((s, timer) -> System.out.println(timer));
    }

    public static void clear() {
        timers.clear();
    }

    public static void flush() {
        printAll();
        clear();
    }

}
