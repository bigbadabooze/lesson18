package zone.haze.restaurant;

import zone.haze.restaurant.data.DB;
import zone.haze.restaurant.data.Options;
import zone.haze.restaurant.kitchen.Kitchen;
import zone.haze.restaurant.kitchen.Order;

public class Restaurant {
    private static Kitchen kitchen;
    private static DB db;

    public static void main(String... args) {
        db = newDB(new Options().parseCommandArguments(args)).connect();
        kitchen = new Kitchen(database());
        kitchen.printRandomMenu();

        Order.orderLoop(database());
    }

    public static DB database() {
        return db;
    }

    public static Kitchen kitchen() {
        return kitchen;
    }

    public static DB newDB(Options options) {
        return DB.Companion.getConnection(options);
    }

}
