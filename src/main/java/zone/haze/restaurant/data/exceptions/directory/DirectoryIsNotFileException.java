package zone.haze.restaurant.data.exceptions.directory;

public class DirectoryIsNotFileException extends IllegalArgumentException {

    public DirectoryIsNotFileException() {
        super();
    }

    public DirectoryIsNotFileException(String s) {
        super(s);
    }

}
