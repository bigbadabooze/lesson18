package zone.haze.restaurant.data.exceptions.file;

import java.io.IOException;

public class FileNotReadableException extends IOException {

    public FileNotReadableException() {
        super();
    }

    public FileNotReadableException(String message) {
        super(message);
    }

}
