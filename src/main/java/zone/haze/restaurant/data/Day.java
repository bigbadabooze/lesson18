package zone.haze.restaurant.data;

import zone.haze.restaurant.kitchen.dish.BaseDish;
import zone.haze.restaurant.kitchen.types.Dish;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Day {
    Monday(0, "Понедельник"),
    Tuesday(1, "Вторник"),
    Wednesday(2, "Среда"),
    Thursday(3, "Четверг"),
    Friday(4, "Пятница", 5),
    Saturday(5, "Суббота", true, 10),
    Sunday(6, "Воскресенье", true, 10);

    private static final Map<Day, HashMap<Integer, Dish>> dishesByDay = new HashMap<>();
    private static final Map<Integer, Day> map = new HashMap<>(Day.values().length);
    private int dayOfWeek;
    private String local;
    private boolean weekend;
    private int discount;

    Day(int dayOfWeekNumber, String dayOfWeekString, boolean isWeekend, int discount) {
        dayOfWeek = dayOfWeekNumber;
        local = dayOfWeekString;
        weekend = isWeekend;
        this.discount = discount;
    }

    Day(int i, String name, int discount) {
        this(i, name, false, discount);
    }

    Day(int i, String name) {
        this(i, name, false, 0);
    }

    public int now() {
        return dayOfWeek;
    }

    public static boolean isDay(String day) {
        return toDay(day) != null;
    }

    private static void dayMapInitialize() {
        Arrays.asList(Day.values()).forEach(day -> {
            map.put(day.now(), day);
        });
    }

    public static Day toDay(String day) {
        if (day == null || day.length() < 5) {
            return null;
        }

        day = day.trim();

        try {
            return valueOf(day);
        } catch (IllegalArgumentException iae) {
            return null;
        }
    }

    public static Day toDay(int day) {
        if (map.get(day) == null) {
            dayMapInitialize();
        }

        return map.get(day);
    }

    public String getLocal() {
        return local;
    }

    public boolean isWeekend() {
        return weekend;
    }

    public int getDiscount() {
        return discount;
    }

    public double calcDiscount() {
        return discount * 0.01;
    }

    public Day addDish(Dish dish) {
        dishesByDay.computeIfAbsent(this, k -> new HashMap<>());
        dishesByDay.get(this).putIfAbsent(((BaseDish) dish).getId(), dish);

        return this;
    }

    public boolean isDishInDay(Dish dish) {
        return dishesByDay.get(this) != null && dishesByDay.get(this).containsKey(((BaseDish) dish).getId());
    }

}
