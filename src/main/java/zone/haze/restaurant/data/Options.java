package zone.haze.restaurant.data;

import java.util.HashMap;

public class Options extends HashMap<String, String> {
    public Options() { /**/ }

    public Options(String... args) {
        parseCommandArguments(args);
    }

    public Options parseCommandArguments(String... args) {
        for (String arg : args) {
            if (!arg.startsWith("--")) continue;

            String[] pair = arg.split("=");

            try {
                put(pair[0].replace("-", ""), pair[1]);
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.format("Плохо сформированный аргумент: %s", arg);
            }
        }

        return this;
    }

}
