package zone.haze.restaurant.data;

import zone.haze.restaurant.utils.struct.Dual;

import java.util.ArrayList;

public class KitchenCollection<C, D> implements Dual {
    private ArrayList<C> cookers;
    private ArrayList<D> dishes;

    public KitchenCollection() {
        cookers = new ArrayList<>();
        dishes = new ArrayList<>();
    }

    public ArrayList<C> getCookers() {
        return cookers;
    }

    public ArrayList<D> getDishes() {
        return dishes;
    }

    public KitchenCollection addCooker(C cooker) {
        cookers.add(cooker);

        return this;
    }

    public KitchenCollection addDish(D dish) {
        dishes.add(dish);

        return this;
    }

}
