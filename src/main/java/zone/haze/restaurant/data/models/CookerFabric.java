package zone.haze.restaurant.data.models;

import org.jetbrains.annotations.NotNull;
import zone.haze.restaurant.kitchen.types.Cooker;

import java.lang.reflect.InvocationTargetException;

public class CookerFabric {

    private static Class getConcreteClass(String dishType) throws InstantiationException {
        try {
            return Class.forName(String.format("zone.haze.restaurant.kitchen.cooker.%s", dishType.trim()));
        } catch (ClassNotFoundException e) {
            throw new InstantiationException(e.getMessage());
        }
    }

    @NotNull
    public static Cooker createCooker(@NotNull String name, @NotNull String specialization) {
        try {
            return (Cooker) getConcreteClass(specialization).getConstructor(String.class).newInstance(name.trim());
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Your data is invalid");
        }
    }

}
