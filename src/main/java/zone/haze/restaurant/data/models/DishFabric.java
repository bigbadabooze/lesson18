package zone.haze.restaurant.data.models;

import org.jetbrains.annotations.NotNull;
import zone.haze.restaurant.kitchen.types.Dish;

import java.lang.reflect.InvocationTargetException;

public class DishFabric {

    private static Class getConcreteClass(String dishType) throws InstantiationException {
        try {
            return Class.forName(String.format("zone.haze.restaurant.kitchen.dish.%s", dishType.trim()));
        } catch (ClassNotFoundException | ExceptionInInitializerError e) {
            throw new InstantiationException(e.toString());
        }
    }

    @NotNull
    public static Dish createDish(@NotNull String title, @NotNull String price, @NotNull String type) {
        try {
            return (Dish) getConcreteClass(type).getConstructor(String.class, String.class).newInstance(title.trim(), price);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

}
