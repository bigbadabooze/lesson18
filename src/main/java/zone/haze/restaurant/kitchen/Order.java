package zone.haze.restaurant.kitchen;

import zone.haze.restaurant.data.DB;
import zone.haze.restaurant.data.KitchenCollection;
import zone.haze.restaurant.kitchen.dish.BaseDish;
import zone.haze.restaurant.kitchen.types.Cooker;
import zone.haze.restaurant.kitchen.types.Dish;
import zone.haze.restaurant.utils.Reader;
import zone.haze.restaurant.Restaurant;
import zone.haze.restaurant.data.Day;

import java.util.HashMap;
import java.util.Arrays;

public class Order extends HashMap<Integer, Dish> {
    private static Reader reader = Reader.INSTANCE;
    private static Order currentOrder;
    private static DB database;

    private HashMap<Integer, Integer> amount = new HashMap<>();
    private HashMap knownDishes = Kitchen.getKnownDishes();
    private double fullOrderPrice = 0.0;
    private Day today = Kitchen.today();

    public static void orderLoop(DB database) {
        orderLoop(new Order(), database);
    }

    public static void orderLoop(Order order, DB db) {
        currentOrder = order;
        database = db;
        String temp;

        while (true) {
            temp = reader.line().replaceAll("\\s", " ");

            if (temp.toLowerCase().equals("done") || temp.toLowerCase().equals("готово")) break;

            if (temp.toLowerCase().equals("order") || temp.toLowerCase().equals("заказ")) {
                order.printFullOrderPrice().forEach(
                        (id, dish) -> System.out.printf(
                                "Блюдо \"%s\" в количестве %d штук по цене %s Итого: %.2fгрн.\n",
                                ((BaseDish) dish).getTitle(),
                                order.amount.get(id),
                                ((BaseDish) dish).getPriceString(),
                                order.amount.get(id) * ((BaseDish) dish).getPrice()
                        )
                );

                continue;
            }

            if (temp.toLowerCase().equals("menu") || temp.toLowerCase().equals("меню")) {
                order.printTodayMenu();

                continue;
            }

            try {
                if (temp.contains(" ")) {
                    Arrays.stream(temp.split(" ")).forEach(orderPart -> resolveLevel(orderPart));
                } else resolveLevel(temp);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                System.out.println("Вы ввели неверное значение. Попробуйте ещё раз. Если вы закончили заказ, наберите done или готово.");
            }
        }

        order.printFullOrderPrice();
    }

    private static void resolveLevel(String temp, String... slicedTemp) {
        if (temp.contains(":")) {
            slicedTemp = temp.split(":");

            resolveOrder(reader.toInt(slicedTemp[0]), reader.toInt(slicedTemp[1]));
        } else {
            resolveOrder(reader.toInt(temp), 1);
        }
    }

    private static void resolveOrder(int id, int amount) {
        if (amount > 0) {
            currentOrder.addToOrder(id, amount);
        } else {
            currentOrder.removeFromOrder(id, amount * -1);
        }
    }

    public Order addToOrder(Integer id, Integer amount) {
        try {
            HashMap data = database.data().get(today);
            Dish localDish = (Dish) knownDishes.get(id);
            KitchenCollection kc = (KitchenCollection) data.get(localDish.type());

            if (kc.getCookers().size() < 1) {
                System.out.println("Сегодня ни один повар не сможет обслужить это блюдо.");

                return this;
            }
        } catch (NullPointerException npe) {
            System.out.println("Похоже, что это технически невозможно.");

            return this;
        }

        if (!knownDishes.containsKey(id) || !today.isDishInDay((Dish) knownDishes.get(id))) {
            System.out.println("Такого блюда не существует или оно не доступно в этот день.");
            return this;
        }

        BaseDish dish;

        if (containsKey(id)) {
            dish = (BaseDish) get(id);
            fullOrderPrice += amount * dish.getPrice();

            this.amount.put(id, this.amount.get(id) + amount);
        } else {
            dish = (BaseDish) knownDishes.get(id);
            super.put(id, (Dish) dish);
            fullOrderPrice += amount * dish.getPrice();

            this.amount.put(id, amount);
        }

        System.out.printf("Количество \"%s\" в заказе: %d штук.\n", (dish.getTitle()), this.amount.get(id));

        return this;
    }

    public Order removeFromOrder(Integer id, Integer amount) {
        if (!knownDishes.containsKey(id) || !this.containsKey(id)) {
            System.out.println("Такого блюда не существует или оно не было найдено в заказе.");
            return this;
        }

        BaseDish dish = (BaseDish) get(id);
        int minus = amount;
        int oldAmount = this.amount.get(id);

        this.amount.put(id, oldAmount - amount);
        amount = oldAmount - amount;

        if (amount > 0) {
            fullOrderPrice -= minus * dish.getPrice();

            System.out.printf("Количество \"%s\" в заказе: %d штук.\n", dish.getTitle(), this.amount.get(id));
        } else {
            remove(id);
            this.amount.remove(id);
            fullOrderPrice -= oldAmount * dish.getPrice();

            System.out.printf("Блюдо \"%s\" было убрано из заказа.\n", dish.getTitle());
        }

        return this;
    }

    public double getFullOrderPrice() {
        return fullOrderPrice;
    }

    public Order printFullOrderPrice() {
        double localPrice = getFullOrderPrice();

        if (today.getDiscount() != 0) {
            localPrice *= today.calcDiscount();
            localPrice = getFullOrderPrice() - localPrice;
        }

        System.out.printf("Общая сумма заказа: %.2fгрн.\n", localPrice);

        return this;
    }

    public Order printTodayMenu() {
        Restaurant.kitchen().printMenu(today.now());

        return this;
    }

}
