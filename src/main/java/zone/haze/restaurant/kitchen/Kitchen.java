package zone.haze.restaurant.kitchen;

import zone.haze.restaurant.data.DB;
import zone.haze.restaurant.data.Day;
import zone.haze.restaurant.kitchen.dish.BaseDish;
import zone.haze.restaurant.kitchen.types.Cooker;
import zone.haze.restaurant.kitchen.types.Dish;

import java.util.HashMap;
import java.util.Random;

public class Kitchen {
    private static HashMap<Integer, Dish> knownDishes = new HashMap<>();
    private static HashMap<Integer, Cooker> knownCookers = new HashMap<>();
    private static Day now;

    private Random random = new Random();
    private DB database;

    public Kitchen(DB db) {
        database = db;
    }

    public void printMenu(int day) {
        now = Day.toDay(day);

        System.out.printf(
                "Сегодня %s.%s%s Актуальное меню:\n",
                now.getLocal().toLowerCase(),
                now.isWeekend() ? " Выходной!" : "",
                now.getDiscount() != 0 ? String.format(" Скидки %d%c на всё!", now.getDiscount(), '%') : ""
        );

        database.data().get(now).forEach(((type, kitchenCollection) -> {
            if (kitchenCollection.getCookers().size() == 0) return;

            System.out.printf("%s:\n", type.getLocal());
            kitchenCollection.getDishes().forEach(System.out::println);
            System.out.println();
        }));
    }

    public void printRandomMenu() {
        printMenu(random.nextInt(Day.values().length));
    }

    public static boolean inKnownDishes(BaseDish dish) {
        return getKnownDishes().containsKey(dish.getId());
    }

    public static HashMap<Integer, Dish> getKnownDishes() {
        return knownDishes;
    }

    public static HashMap<Integer, Cooker> getKnownCookers() {
        return knownCookers;
    }

    public static Day today() {
        return now;
    }

}
