package zone.haze.restaurant.kitchen.types;

public enum DishType {
    FirstDish("Первое"),
    SecondDish("Второе"),
    Dessert("Десерт"),
    Drink("Напиток");

    private String local;

    DishType(String local) {
        this.local = local;
    }

    public static boolean isDishType(String type) {
        return toDishType(type) != null;
    }

    public static DishType toDishType(String dishType) {
        if (dishType == null || dishType.length() < 4) {
            return null;
        }

        try {
            return valueOf(dishType);
        } catch (IllegalArgumentException iae) {
            return null;
        }
    }

    public String getLocal() {
        return local;
    }

}
