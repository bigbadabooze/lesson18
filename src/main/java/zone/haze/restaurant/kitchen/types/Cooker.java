package zone.haze.restaurant.kitchen.types;

import zone.haze.restaurant.kitchen.types.DishType;

public interface Cooker {

    DishType specialization();

}
