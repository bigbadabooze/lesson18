package zone.haze.restaurant.kitchen.dish;

import zone.haze.restaurant.kitchen.types.Dish;
import zone.haze.restaurant.kitchen.types.DishType;

public class Dessert extends BaseDish implements Dish {
    private static final DishType type = DishType.Dessert;

    public Dessert(String title, String price) {
        super(title, price);
    }

    public Dessert(String title, Double price) {
        super(title, price);
    }

    @Override
    public DishType type() {
        return type;
    }

}
