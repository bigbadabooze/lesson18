package zone.haze.restaurant.kitchen.dish;

import zone.haze.restaurant.data.DB;
import zone.haze.restaurant.data.Day;
import zone.haze.restaurant.kitchen.Kitchen;
import zone.haze.restaurant.kitchen.types.Dish;

import java.util.HashMap;

public abstract class BaseDish {
    private static volatile int currentID = 0;
    private static final Character hrn = '₴';
    private static HashMap knownDishes = Kitchen.getKnownDishes();

    public static synchronized int getGlobalID() {
        return currentID;
    }

    private static synchronized int getAndIncrementCurrentID() {
        return ++currentID;
    }

    private int id;
    private String title;
    private Integer priceIntegerPart;
    private Integer priceFractionalPart;

    public BaseDish(String title, String price) {
        id = getAndIncrementCurrentID();
        this.title = title;

        String[] parts = price.split("\\.", 2);
        priceIntegerPart = Integer.parseInt(parts[0]);
        priceFractionalPart = Integer.parseInt(parts[1]);

        knownDishes.put(id, this);
        addSelfToDayMap();
    }

    public BaseDish(String title, Double price) {
        this(title, String.valueOf(price));
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Double getPrice() {
        return (double) priceIntegerPart + (double) priceFractionalPart * 0.01;
    }

    public String getPriceString() {
        return String.format("%dгрн. %dкоп.", priceIntegerPart, priceFractionalPart);
    }

    @Override
    public String toString() {
        return String.format(
                "%d) \"%s\" %s",
                getId(),
                getTitle(),
                getPriceString()
        );
    }

    private void addSelfToDayMap() {
        Day day = DB.Companion.getCurrentDay();

        day.addDish((Dish) this);
    }

}
