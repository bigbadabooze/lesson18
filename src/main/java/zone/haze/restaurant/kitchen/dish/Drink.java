package zone.haze.restaurant.kitchen.dish;

import zone.haze.restaurant.kitchen.types.Dish;
import zone.haze.restaurant.kitchen.types.DishType;

public class Drink extends BaseDish implements Dish {
    private static final DishType type = DishType.Drink;

    public Drink(String title, String price) {
        super(title, price);
    }

    public Drink(String title, Double price) {
        super(title, price);
    }

    @Override
    public DishType type() {
        return type;
    }

}
