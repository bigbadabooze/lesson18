package zone.haze.restaurant.kitchen.dish;

import zone.haze.restaurant.kitchen.types.Dish;
import zone.haze.restaurant.kitchen.types.DishType;

public class SecondDish extends BaseDish implements Dish {
    private static final DishType type = DishType.SecondDish;

    public SecondDish(String title, String price) {
        super(title, price);
    }

    public SecondDish(String title, Double price) {
        super(title, price);
    }

    @Override
    public DishType type() {
        return type;
    }

}
