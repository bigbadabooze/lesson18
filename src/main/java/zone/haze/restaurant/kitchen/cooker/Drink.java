package zone.haze.restaurant.kitchen.cooker;

import zone.haze.restaurant.kitchen.types.Cooker;
import zone.haze.restaurant.kitchen.types.DishType;

public class Drink extends BaseCooker implements Cooker {
    private static final DishType specialization = DishType.Drink;

    public Drink(String name) {
        super(name);
    }

    @Override
    public DishType specialization() {
        return specialization;
    }

}
