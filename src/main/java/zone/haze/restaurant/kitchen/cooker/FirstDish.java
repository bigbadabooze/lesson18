package zone.haze.restaurant.kitchen.cooker;

import zone.haze.restaurant.kitchen.types.Cooker;
import zone.haze.restaurant.kitchen.types.DishType;

public class FirstDish extends BaseCooker implements Cooker {
    private static final DishType specialization = DishType.FirstDish;

    public FirstDish(String name) {
        super(name);
    }

    @Override
    public DishType specialization() {
        return specialization;
    }

}
