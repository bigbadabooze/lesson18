package zone.haze.restaurant.kitchen.cooker;

import zone.haze.restaurant.kitchen.types.Cooker;
import zone.haze.restaurant.kitchen.types.DishType;

public class Dessert extends BaseCooker implements Cooker {
    private static final DishType specialization = DishType.Dessert;

    public Dessert(String name) {
        super(name);
    }

    @Override
    public DishType specialization() {
        return specialization;
    }

}
