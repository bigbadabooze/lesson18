package zone.haze.restaurant.kitchen.cooker;

import zone.haze.restaurant.kitchen.types.Cooker;
import zone.haze.restaurant.kitchen.types.DishType;

public class SecondDish extends BaseCooker implements Cooker {
    private static final DishType specialization = DishType.SecondDish;

    public SecondDish(String name) {
        super(name);
    }

    @Override
    public DishType specialization() {
        return specialization;
    }

}
