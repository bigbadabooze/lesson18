package zone.haze.restaurant.kitchen.cooker;

import zone.haze.restaurant.kitchen.Kitchen;

import java.util.HashMap;

public abstract class BaseCooker {
    private static volatile int currentID = 0;
    private static HashMap knownCookers = Kitchen.getKnownCookers();

    public static synchronized int getGlobalID() {
        return currentID;
    }

    private static synchronized int getCurrentID() {
        return currentID++;
    }

    private int id;
    private String name;

    public BaseCooker(String name) {
        id = getCurrentID();
        this.name = name;

        knownCookers.put(id, this);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }

}
